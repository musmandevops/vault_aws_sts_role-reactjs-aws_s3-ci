# CI/CD with Vault , AWS S3 and AWS STS
## In this example I have configured CI/CD pipeline which do the following things
1. test/build react application
2. get temporary AWS role base credentials from vault
3. deploy react to AWS s3